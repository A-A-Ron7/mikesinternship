<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#mikes-internship">1. Mike's Internship</a>
<ul>
<li><a href="#org38e189c">1.1. This repository will document our progress throughout the summer</a></li>
<li><a href="#download-repository">1.2. Download repository</a></li>
<li><a href="#resources">1.3. Resources</a></li>
</ul>
</li>
</ul>
</div>
</div>

<a id="mikes-internship"></a>

# Mike's Internship


<a id="org38e189c"></a>

## This repository will document our progress throughout the summer

semester

:CUSTOM<sub>ID</sub>: this-repository-will-document-our-progress-throughout-the-summer-semester


<a id="download-repository"></a>

## Download repository

So long as the user has git installed on their system, the following
command can be used to clone the repository:

    git clone https://Heliosrx2@bitbucket.org/Heliosrx2/mikesinternship.git


<a id="resources"></a>

## Resources

The [Skanect](http://skanect.occipital.com/download/) program allows
the structure sensor to the send the scan to another system. The
[
PyWavefront](https://github.com/greenmoss/PyWavefront) allows .obj files to be viewed using a python script. However the library does have limitations.

