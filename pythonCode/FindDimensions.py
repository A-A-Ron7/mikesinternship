input_file = open("ZorroFullBody.txt", "r")
vertices = []
xList = []
yList = []
zList = []
x_min = 0
x_max = 0
y_min = 0
y_max = 0
z_min = 0
z_max = 0
objectMeasurements = []

for line in input_file:
    vertices.append(line)

for i in range(0, len(vertices)):
    vertex = vertices[i].split(" ")
    for j in range(0, len(vertex)):
        if j == 0:
            xList.append(vertex[j]) 
        if j == 1:
            yList.append(vertex[j])
        if j == 2:
            zList.append(vertex[j])

def xMinMax(xMin, xMax):
  for i in range(0, len(xList)):
    xList[i].rstrip()
    x_float = float(xList[i])
    if x_float > xMax:
      xMax = x_float
    elif x_float < xMin:
      xMin = x_float
  ##print ("x min: ", xMin)
  ##print ("x max: ", xMax)
  Xlist = [xMin, xMax]
  return Xlist

def yMinMax(yMin, yMax):
  for i in range(0, len(yList)):
    yList[i].rstrip()
    y_float = float(yList[i])
    if y_float > yMax:
      yMax = y_float
    elif y_float < yMin:
      yMin = y_float
  ##print ("y min: ", yMin)
  ##print ("y max: ", yMax)
  Ylist = [yMin, yMax]
  return Ylist

def zMinMax(zMin, zMax):
  for i in range(0, len(zList)):
    zList[i].rstrip()
    z_float = float(zList[i])
    if z_float > zMax:
      zMax = z_float
    elif z_float < zMin:
      zMin = z_float
  ##print ("z min: ", zMin)
  ##print ("z max: ", zMax)
  Zlist = [zMin, zMax]
  return Zlist

minmaxxlist = xMinMax(x_min, x_max)
minmaxylist = yMinMax(y_min, y_max)
minmaxzlist = zMinMax(z_min, z_max)

x_min = minmaxxlist[0]
x_max = minmaxxlist[1]
y_min = minmaxylist[0]
y_max = minmaxylist[1]
z_min = minmaxzlist[0]
z_max = minmaxzlist[1]
measurex = round(abs(x_min) + x_max, 3)
measurey = round(abs(y_min) + y_max, 3)
measurez = round(abs(z_min) + z_max, 3)
objectMeasurements.append(measurex)
objectMeasurements.append(measurey)
objectMeasurements.append(measurez)
##print("Head + Torso")
##print ("Width: ", objectMeasurements[0], "meters")
##print ("Length: ", objectMeasurements[1], "meters")
##print ("Height: ", objectMeasurements[2], "meters")


"""
measureBodyPart() serves as a function that can be called
to target a specific body part
"""
minimumNeckX = 0
maximumNeckX = 0
minimumNeckY = 0
maximumNecky = 0
def measureBodyPart(locationStartZ):
    global minimumNeckX
    global maximumNeckX
    lowerBoundZ = (locationStartZ - .0002)
    upperBoundZ = (locationStartZ + .0002)
    locationStartZ = lowerBoundZ
    while(locationStartZ <= upperBoundZ):
        for i in range(len(zList)):
            if (locationStartZ == round(float(zList[i]),5)):
                if (round(float(xList[i]),5) < float(minimumNeckX)):
                    minimumNeckX = xList[i]
                if (round(float(xList[i]),5) > float(maximumNeckX)):
                    maximumNeckX = xList[i]
                break
        locationStartZ = round(locationStartZ + .00001, 5)
measureBodyPart(.65821)
neckWidth = ((abs(float(minimumNeckX)) + float(maximumNeckX)) / objectMeasurements[0]) * objectMeasurements[0]
print("Neck Width: ", round(neckWidth,5), "meters")

"""
This function returns a percentage of hits to misses
hitRatio / total --- use this number to multiply it by
the actual length of the bounding box to determine the
real life length.

05/23/17 UPDATE - This method is now irrelevant and has been
replaced by a more efficient process (directly above this
block comment)

def dimensions(minx, maxx, miny, maxy, minz, maxz, xList, yList, zList):
    hitX = 0
    hitY = 0
    hitZ = 0
    hitRatioX = 0.00
    hitRatioY = 0.00
    hitRatioZ = 0.00
    i = minx
    while i < maxx:
        for j in range(len(xList)):
            if (i == (round(float(xList[j]), 5))):
                hitX += 1
        i = round(i + .00001, 5)
    i = miny
    while i < maxy:
        for j in range(len(yList)):
            if (i == (round(float(yList[j]),5))):
                hitY += 1
        i = round(i + .00001, 5)
    i = minz
    while i < maxz:
        for j in range(len(zList)):
            if (i == (round(float(zList[j]),5))):
                hitZ += 1
        i = round(i + .00001, 5)
    hitRatioX = (hitX/len(xList))
    hitRatioY = (hitY/len(yList))
    hitRatioZ = (hitZ/len(zList))
    print("X-ratio: ", round(hitRatioX,5))
    print("Y-ratio: ", round(hitRatioY,5))
    print("Z-ratio: ", round(hitRatioZ,5))

dimensions(round(x_min,5), round(x_max,5), round(y_min,5), round(y_max,5), round(z_min,5), z_max, xList, yList, zList)
"""
